# Modèle Logique de Données

Toutes les tables respectent une normalisation 3NF au minimum afin d'éviter la redondance.

=> Héritage par la classe mère pour les classes filles de Vehicule


- **Vehicule**(#immatriculation : varchar, marque : varchar, annee : integer, modele : varchar, capaciteMoteur : integer, capaciteMax: integer, type : voiture_spe, transporteur => Camion) avec marque, annee et modele NOT NULL.
    - 1NF : chaque cellule contient une seule valeur + chaque ligne est unique 
    - 2NF : 1NF + clé primaire simple
    - 3NF : 2NF + pas de dépendance fonctionnelle (peut etre avec la plaque d'immatriculation pr les véhicules spéciaux ?)

Contraintes : 

- *Voitures* :
capaciteMax IS NULL AND capaciteMoteur IS NULL AND type IS NULL
- *Moto* : 
capaciteMax IS NULL AND capaciteMoteur IS NOT NULL AND type IS NULL AND transporteur IS NULL
- *Camion* :
capaciteMax IS NOT NULL AND capaciteMoteur IS NULL AND type IS NULL AND transporteur IS NULL
- *Voiture Spé* : 
capaciteMax IS NULL AND capaciteMoteur IS NULL AND type IS NOT NULL AND transporteur IS NULL

----------------------------------------------------------------------------------------- 

=> Héritage par la classe fille

- **AlerteMeteo**(#id : serial, timeStamp : date, capteur => Capteur, temps : temps, temperature: integer, commune=> Commune) avec temps NOT NULL, timeStamp NOT NULL, capteur NOT NULL et commune NOT NULL
    - 1NF : chaque cellule contient une seule valeur + chaque ligne est unique 
    - 2NF : 1NF + clé primaire simple

- **Accident**(#id : serial, timeStamp : date, capteur => Capteur, commune=> Communne, gravite : gravite, vehicule => Vehicule) avec timeStamp NOT NULL, gravite NOT NULL, vehicule NOT NULL, capteur NOT NULL et commune NOT NULL
    - 1NF : chaque cellule contient une seule valeur + chaque ligne est unique 
    - 2NF : 1NF + clé primaire simple

----------------------------------------------------------------------------------------- 

=> Dans le cadre de la dernière semaine, j'ai simplifié le modèle afin d'utiliser le JSON.

- **Evenement**(#id : serial, timeStamp : date, capteur => Capteur, type : varchar, contenu : JSON {"temps", "temperature", "commune"} | {"communne", "gravite", "immat"})
    - 1NF : chaque cellule contient une seule valeur + chaque ligne est unique 
    - 2NF : 1NF + clé primaire simple
    - Pas 3NF car il existe des dépendances fonctionnelles.
=> Autres

- **Commune**(#id : serial, nom : varchar, codePostal : integer) 
    - 1NF : chaque cellule contient une seule valeur + chaque ligne est unique 
    - 2NF : 1NF + clé primaire simple 
    - pas de 3NF car dépendance fonctionnelle nom => codepostal

- **StationBase**(#id_station : integer, longitude : varchar, latitude : varchar) avec latitude NOT NULL et longitude NOT NULL
    - 1NF : chaque cellule contient une seule valeur + chaque ligne est unique 
    - 2NF : 1NF + clé primaire simple 
    - pas de 3NF car dépendance fonctionnelle lat/long => idcommune


Contraintes : StationBase.id_station NOT NULL et PROJECTION(Commune, id_station) = PROJECTION(StationBase, id_station)

- **Capteur**(#num : integer, modele : varchar, station => StationBase, véhicule => Vehicule) avec (vehicule IS NULL AND station is NOT NULL) OR (vehicule IS NOT NULL AND station is NULL)
    - 1NF : chaque cellule contient une seule valeur + chaque ligne est unique 
    - 2NF : 1NF + clé primaire simple 
    - 3NF : pas de dépendance fonctionnelle

- **Communication**(#immatriculation => Vehicule, #id_station => StationBase, contenu : varchar, type : varchar, timeStamp : Date ) avec contenu NOT NULL, type NOT NULL et timeStamp NOT NULL
    - 1NF : chaque cellule contient une seule valeur + chaque ligne est unique 
    - 2NF : 1NF + clé primaire simple 
    - pas 3NF car dépendance fonctionnelle entre contenu => typecontenu et position => idcommune


## Contraintes relationnelles

- StationBase.id_station NOT NULL et PROJECTION(Commune, id_station) = PROJECTION(StationBase, id_station)
- Accident.id et AlerteMétéo.id NOT NULL et PROJECTION(Accident, id) = PROJECTION(Commune, id) et PROJECTION(AlerteMeteo, id) = PROJECTION(Commune, id) 
- PROJECTION(Accidente, idaccident) = PROJECTION(Accident, id) AND PROJECTION(Accidente, immatvehicule) = PROJECTION(Vehicule, immatriculation)
- PROJECTION(Communication, immatvehicule) = PROJECTION(Vehicule, immatriculation) AND PROJECTION(Communication, idstation) = PROJECTION(StationBase, id station)
- PROJECTION(Capteur, num) = PROJECTION(altermeteo, capteur) AND PROJECTION(Capteur, num) = PROJECTION(accident, capteur)
