    CREATE TYPE VOITURE_SPE as ENUM ( 'pompiers', 'police', 'SAMU');
    CREATE TYPE TEMPS as ENUM ( 'pluie', 'neige', 'brouillard', 'soleil');
    CREATE TYPE GRAVITE as ENUM ( 'bas', 'moyen', 'eleve');


    CREATE TABLE Vehicule (
        immatriculation VARCHAR NOT NULL,
        modele JSON NOT NULL,
        capaciteMoteur INTEGER,
        capaciteMax INTEGER,
        type VOITURE_SPE,
        transporteur VARCHAR,
        PRIMARY KEY (immatriculation),
        FOREIGN KEY (transporteur) REFERENCES Vehicule(immatriculation),
        CHECK ((capaciteMax IS NOT NULL AND capaciteMoteur IS NULL AND type IS NULL AND transporteur IS NULL) OR
        (capaciteMax IS NULL AND capaciteMoteur IS NULL AND type IS NULL AND transporteur IS NOT NULL) OR
            (capaciteMax IS NULL AND capaciteMoteur IS NOT NULL AND type IS NULL AND transporteur IS NULL) OR
            (capaciteMax IS NULL AND capaciteMoteur IS NULL AND type IS NOT NULL AND transporteur IS NULL) OR 
            (capaciteMax IS NULL AND capaciteMoteur IS NULL AND type IS NULL AND transporteur IS NULL))
    );


    CREATE TABLE Commune(
        idCommune SERIAL,
        nom VARCHAR,
        codePostal INTEGER,
        UNIQUE(idCommune),
        PRIMARY KEY (idCommune, nom, codePostal)
    );

    CREATE TABLE StationBase (
        id_station SERIAL,
        longitude VARCHAR NOT NULL,
        latitude VARCHAR NOT NULL,
        idCommune INTEGER,
        PRIMARY KEY (id_station),
        FOREIGN KEY (idCommune) REFERENCES Commune(idCommune)
    );


    CREATE TABLE Capteur (
        num SERIAL,
        modele VARCHAR NOT NULL,
        idStation INTEGER,
        immatVehicule VARCHAR,
        PRIMARY KEY (num),
        FOREIGN KEY (idStation) REFERENCES StationBase(id_station),
        FOREIGN KEY (immatVehicule) REFERENCES Vehicule(immatriculation),
        CHECK ((immatVehicule IS NULL AND idStation is NOT NULL) OR (immatVehicule IS NOT NULL AND idStation is NULL))
    );

    CREATE TABLE Evenement (
        id SERIAL,
        timeStamp DATE NOT NULL,
        capteur INTEGER NOT NULL,
        type varchar NOT NULL,
        contenu JSON NOT NULL,
        PRIMARY KEY (id),
        FOREIGN KEY (capteur) REFERENCES Capteur(num)
    );



    CREATE TABLE Communication (
        immatVehicule VARCHAR,
        idStation INTEGER,
        contenu VARCHAR NOT NULL,
        typeContenu VARCHAR NOT NULL,
        timeStamp DATE NOT NULL,
        position JSON NOT NULL,
        idCommune INTEGER NOT NULL,
        PRIMARY KEY (immatVehicule, idStation),
        FOREIGN KEY (immatVehicule) REFERENCES Vehicule(immatriculation),
        FOREIGN KEY (idStation) REFERENCES StationBase(id_station)
    );

    CREATE TABLE UserTable (
        login VARCHAR,
        pwd VARCHAR NOT NULL,
        privilege INTEGER, -- 1 = yes, 0 = no
        PRIMARY KEY (login)
    );

    -- ===========================================================================
    --                          GESTION DES VUES                                --
    -- ===========================================================================

    -- Lister les véhicules passé par une région, on peut aussi rajouter une contrainte sur le timestamp

    CREATE VIEW vueVoitureRegionPalaiseau AS
        SELECT V.immatriculation, V.modele->>'marque' AS MARQUE, CAST(V.modele->>'annee' AS INTEGER) AS annee , V.modele->>'modele' AS MODELE
        FROM VEHICULE AS V
        INNER JOIN COMMUNICATION AS C ON V.immatriculation = C.immatvehicule
        INNER JOIN COMMUNE as Co ON C.idcommune = Co.idcommune
        WHERE Co.nom = 'Palaiseau';

    -- COMMUNICATION DEPUIS UNE STATION

    CREATE VIEW vueCommStationVehicule AS 
        SELECT C.typecontenu, C.immatvehicule, C.contenu, C.timestamp
        FROM COMMUNICATION AS C
        INNER JOIN STATIONBASE AS SB ON C.idstation = SB.id_station
        WHERE C.idstation= 1;


    --COMMUNICATION LIEES A UNE VOITURE


    CREATE VIEW vueCommVoiture AS
        SELECT typecontenu, contenu, timestamp, position-->"latitude", position-->"longitude"
        FROM COMMUNICATION AS C 
        WHERE C.immatvehicule = 'AA-229-A2';