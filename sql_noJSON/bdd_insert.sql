--Vehicules
    -- voiture
    INSERT INTO Vehicule("immatriculation", "modele") VALUES ('AA-229-AA', '{"marque":"Ford","annee" : 2019, "modele" : "Mondeo" }');
    INSERT INTO Vehicule("immatriculation", "modele") VALUES ('DA-784-FG', '{"marque":"Renault","annee" : 2015, "modele" : "Clio" }');
    INSERT INTO Vehicule("immatriculation", "modele") VALUES ('DA-784-GK', '{"marque":"Renault","annee" : 2012, "modele" : "Captur" }');
    INSERT INTO Vehicule("immatriculation", "modele") VALUES ('VC-524-AS', '{"marque":"Peugeot","annee" : 2007, "modele" : "306" }');
    INSERT INTO Vehicule("immatriculation", "modele") VALUES ('FL-648-ML', '{"marque":"Audi","annee" : 2019, "modele" : "A6" }');
    INSERT INTO Vehicule("immatriculation", "modele") VALUES ('ML-229-NB', '{"marque":"BMW","annee" : 2015, "modele" : "E36" }');
    INSERT INTO Vehicule("immatriculation", "modele") VALUES ('WD-520-WA', '{"marque":"Mercedes","annee" : 2019, "modele" : "classe C" }');
    INSERT INTO Vehicule("immatriculation", "modele") VALUES ('AQ-245-ZS', '{"marque":"Fiat","annee" : 2019, "modele" : "500" }');
    -- moto
    INSERT INTO Vehicule("immatriculation", "modele", "capacitemoteur") VALUES ('AA-229-A2', '{"marque":"Dugati","annee" : 2019, "modele" : "Zat" }', 120);
    INSERT INTO Vehicule("immatriculation", "modele", "capacitemoteur") VALUES ('MP-789-AC', '{"marque":"Triumph","annee" : 2019, "modele" : "1200RS" }', 300);
    INSERT INTO Vehicule("immatriculation", "modele", "capacitemoteur") VALUES ('AZ-457-ER', '{"marque":"Yamaha","annee" : 2019, "modele" : "700" }', 150);
    --camion
    INSERT INTO Vehicule("immatriculation", "modele", "capacitemax") VALUES ('AD-654-AQ','{"marque":"Volvo","annee" : 2019, "modele" : "Bigtruck" }', 10);
    --voiture spé : SAMU
    INSERT INTO Vehicule("immatriculation", "modele", "type") VALUES ('VS-676-AQ', '{"marque":"Renault","annee" : 2019, "modele" : "Pimpon" }', 'SAMU');
    INSERT INTO Vehicule("immatriculation", "modele", "type") VALUES ('HS-745-AQ', '{"marque":"Peugeot","annee" : 2019, "modele" : "Truck" }', 'police');
    INSERT INTO Vehicule("immatriculation", "modele", "type") VALUES ('QX-799-AQ', '{"marque":"Ford","annee" : 2019, "modele" : "Big Pompiers" }', 'pompiers');
    --voiture transportée par Camion (AA-229-AQ)
    INSERT INTO Vehicule("immatriculation", "modele", "transporteur") VALUES ('FF-229-AQ', '{"marque":"Peugoet","annee" : 2019, "modele" : "GTLINE 508" }', 'AD-654-AQ');

--Commune
    INSERT INTO Commune("nom", "codepostal") VALUES ('Palaiseau', 91120);
    INSERT INTO Commune("nom", "codepostal") VALUES ('Compiègne', 60200);
    INSERT INTO Commune("nom", "codepostal") VALUES ('Orsay', 91400);

--StationBase
    INSERT INTO StationBase("longitude", "latitude", "idcommune") VALUES ('48.8534', '48.8534', 1);
    INSERT INTO StationBase("longitude", "latitude", "idcommune") VALUES ('48.8538', '48.8568', 1);
    INSERT INTO StationBase("longitude", "latitude", "idcommune") VALUES ('48.8584', '48.8554', 2);
    INSERT INTO StationBase("longitude", "latitude", "idcommune") VALUES ('48.8654', '48.6634', 2);
    INSERT INTO StationBase("longitude", "latitude", "idcommune") VALUES ('48.8684', '48.3201', 3);
    INSERT INTO StationBase("longitude", "latitude", "idcommune") VALUES ('48.8654', '48.9872', 3);

--Capteur
    --véhicule
    INSERT INTO Capteur("modele", "immatvehicule") VALUES ('BCC545','AA-229-AA');
    INSERT INTO Capteur("modele", "immatvehicule") VALUES ('BCC546','DA-784-FG');
    INSERT INTO Capteur("modele", "immatvehicule") VALUES ('BCC547','DA-784-GK');
    INSERT INTO Capteur("modele", "immatvehicule") VALUES ('BCC548','VC-524-AS');
    INSERT INTO Capteur("modele", "immatvehicule") VALUES ('BCC549','FL-648-ML');
    INSERT INTO Capteur("modele", "immatvehicule") VALUES ('BCC550','ML-229-NB');
    INSERT INTO Capteur("modele", "immatvehicule") VALUES ('BCC551','WD-520-WA');
    --StationBase
    INSERT INTO Capteur("modele", "idstation") VALUES ('BCC599',1);

--evenements 
    --alertemeteo
    INSERT INTO alertemeteo("timestamp", "capteur", "temps", "temperature", "idcommune") VALUES ('2020-04-30', 9, 'soleil', 25, 1);
    INSERT INTO alertemeteo("timestamp", "capteur", "temps", "temperature", "idcommune") VALUES ('2020-08-6', 9, 'pluie', 25, 1);
    INSERT INTO alertemeteo("timestamp", "capteur", "temps", "temperature", "idcommune") VALUES ('2020-09-12', 9, 'neige', 25, 1);
    --accident
    INSERT INTO accident("timestamp", "capteur", "idcommune", "gravite") VALUES ('2020-04-9', 1, 1, 'eleve');
    INSERT INTO accidente VALUES ('ML-229-NB', 1);
    INSERT INTO accidente VALUES ('FL-648-ML', 1);

--communication 
    INSERT INTO communication VALUES ('AA-229-A2', 1, '125', 'Vitesse', '2020-04-9', '{"latitude" : "45.6565", "longitude" : "48.5465"}', 1);

--User
INSERT INTO UserTable VALUES('admin', 'mypwd', 1);
INSERT INTO UserTable VALUES('lambdaUser', 'otherpwd', 0);