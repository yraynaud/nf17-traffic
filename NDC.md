# Note de clarification

## Liste des objets

Voici la liste des objets nécéssaire pour la réalisation de ce projet. 

- **Véhicule** : immatriculation (primary), marque, année, modèle
- **Voiture** : 
- **Moto** : capacitéMoteur
- **Camion** : capacitéMax
- **VoitureSpé** : type


- **Évènement** : id (primary), timeStamp
- **AlerteMétéo** : temps, température
- **Accident** : gravité


- **Capteur** : num (primary), modèle
- **Commune** : nom, codePostal
- **StationBase** : id (primary), longitude, latitude
- **Communication** : contenu, type, timeStamp


## Listes des contraintes 

- **Voiture**, **Moto** et **Camion** héritent de **Véhicule**
- **VoitureSpé** hérite de **Voiture**
- **AlerteMétéo** et **Accident** héritent de **Évènement**

- **Camion** est associé à **Voiture**
- **Communication** est associé à **Véhicule** et StationBase**
- **Stationbase** est associée à **Commune**
- **Commune** est associé à **Évènement**
- **Évènement** est associé à **Capteur**
- **Capteur** est associé à **Véhicule**
- **Accident** est associé à **Véhicule**

## Liste des utilisateurs

La *UserStory* ne présente pas de partie spécifique à la gestion des utilisateurs. On pourra alors en définir 2, par sécurité : 
- un admin
- un utilisateur.

## Liste des fonctions

- Lister tous les véhicules dans une région
    - vueVoitureRegionPalaiseau

- Lister toutes les communications liées à une véhicule ou à une station de base
    - vueCommStationVehicule
    - vueStationAlerteMeteo
    - vueStationAccident
    - vueVoitureAccident
    - vueCommVoiture

- Trouver le véhicule le plus proche d'un certain type, par exemple, un camion de pompiers, véhicule SAMU, etc. ou un certain modèle : toutes les Citroën C5, ...
    - vueVehiculeSpecial
- Calculer les statistiques de passage de véhicules : par zone, par type, ...
    - vueVehiculeBase
    - vueVehiculeTypeBase
- Supprimer de la base toutes les communications qui concernent un certain modèle du capteur (par exemple, dont le mal-fonctionnement a été démontré).
    - delete from
- Supprimer de la base toutes les communications qui concernent une station de base particulière (modèle défaillant).
    - delete from
