# NF17 - traffic

## Description générale

L'idée est de créer une base de données qui contiendrait des informations qui circulent dans l'infrastructure V2I (Vehicle-To-Infrastructure) dans un réseau de véhicules de nouvelle génération.
Hypothèses

Le réseau est constitué des nœuds : des véhicules et de l'infrastructure routière (stations de base).

Les véhicules peuvent envoyer des informations vers l'infrastructure. La communication est de type un-à-un.

Tout véhicule est identifié par son numéro d'immatriculation. On lui associe aussi la marque, le modèle et l'année de production.

On distingue plusieurs types de véhicules : voitures, motos, camions, voitures spéciales (pompiers, police, SAMU, etc.).

Certains camions peuvent transporter des voitures et possèdent une capacité maximale.

Les motos ont l'information sur la capacité du moteur.

Les stations de base peuvent communiquent entre elles, ainsi qu'envoyer des informations aux véhicules. La communication dans ce cas peut être de type un-à-un ou un-à-plusieurs.

Chaque station de base a un identifiant unique.

Chaque station a une position fixe (longitude, latitude) et est lié à une commune.

Chaque nœud peut être équipé d'un ou plusieurs capteurs. Les stations de base contiennent forcément au moins un capteur.

Les capteurs détectent des événements divers : accidents, alertes météo, travaux routiers, détection du matériel de contrôle routier.

Chaque capteur est identifié par le modèle et le numéro de série.

Un capteur peut être installé sur un seul nœud en même temps, mais il peut être ajouté, transféré ou supprimé.

Chaque communication entre des nœuds produit un événement qui est stocké dans la base avec des informations comme : le timestamp (horodatage) de l'événement, identifiants des entités communicantes, le type d'échange et le contenu d'échange (dépendant du type), la commune dans laquelle l'événement a eu lieu.

Les véhicules communiquent aux stations de base les informations concernant leur position géographique (latitude, longitude), l'année de production, la marque et le modèle, ainsi que la marque et le modèle du capteur.

Les communes possèdent un nom et un code postal (principal).

Les événements possèdent du contenu différent :

Ils contiennent l'information sur la commune dans laquelle ils ont eu lieu.

Alertes météo : temps (pluie, neige, brouillard, etc.), température

Accident : gravité, nombre et types de véhicules accidentés.

Besoins

Lister tous les véhicules dans une région

Lister toutes les communications liées à une véhicule ou à une station de base

Trouver le véhicule le plus proche d'un certain type, par exemple, un camion de pompiers, véhicule SAMU, etc. ou un certain modèle : toutes les Citroën C5, ...

Calculer les statistiques de passage de véhicules : par zone, par type, ...

Supprimer de la base toutes les communications qui concernent un certain modèle du capteur (par exemple, dont le mal-fonctionnement a été démontré).

Supprimer de la base toutes les communications qui concernent une station de base particulière (modèle défaillant).
