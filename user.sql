CREATE LOGIN admin WITH PASSWORD = 'pwdAdmin';
CREATE LOGIN user WITH PASSWORD = 'pwdUser';
CREATE USER admin FOR LOGIN admin;
CREATE USER user FOR LOGIN user;

-- allow user to select and delete entry for every table
GRANT SELECT, DELETE ON bddtrafic.* TO user;

-- grant all privileges on all table of the db to Admin with grant option to create more specific user if needed
GRANT ALL PRIVILEGES ON bddtrafic.* TO admin WITH GRANT OPTION;

